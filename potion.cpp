#include "potion.h"

Potion::Potion()
{
	timer = 120;
	lvl = 6;
	lvlEffect = 2;
}

int Potion::get_timer()
{
	return timer;
}

int Potion::get_lvl()
{
	return lvl;
}

int Potion::get_lvlEffect()
{
	return  lvlEffect;
}

void Potion::set_timer(int new_timer)
{
	timer = new_timer;
}

void Potion::set_lvl(int new_lvl)
{
	lvl = new_lvl;
}

void Potion::set_lvlEffect(int new_lvlEffect)
{
	lvlEffect = new_lvlEffect;
}



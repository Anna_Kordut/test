#include "armor.h"

Armor::Armor()
{
	power = 0;
	lvl = 0;
	size = 0;
}

int Armor::get_size() {
	return size;
}
int Armor::get_power() {
	return power;
}
bool Armor::get_lvl() {
	return lvl;
}
void Armor::set_size(int new_size) {
	size = new_size;
}
void Armor::set_power(int new_set_power) {
	power = new_set_power;
}
void Armor::set_lvl(int new_lvl) {
	lvl = new_lvl;
}

#pragma once
class Potion
{
	int timer;
	int lvl;
	int lvlEffect;
public:
	Potion();
	int get_timer();
	int get_lvl();
	int get_lvlEffect();
	void set_timer(int new_timer);
	void set_lvl(int new_lvl);
	void set_lvlEffect(int new_lvlEffect);
};


#include "weapon.h"

Weapon::Weapon()
{
	width = 10;
	height = 10;
	lvl = 2;
	force = 5;
}

int Weapon::get_width() {
	return width;
}
int Weapon::get_height() {
	return height;
}
int Weapon::get_force() {
	return force;
}
int Weapon::get_lvl() {
	return lvl;
}
void Weapon::set_width(int new_width) {
	width = new_width;
}
void Weapon::set_height(int new_height){
	height = new_height;
}
void Weapon::set_lvl(int new_lvl) {
	lvl = new_lvl;
}

void Weapon::set_force(int new_force) {
	force = new_force;
}

#include "Mary_player.h"

Mary::Mary() {
	width = 10;
	height = 10;
	beautiful = true;
	number_legs = 2;
}
Potion Mary::get_potion()
{
	return potion;
}
Armor Mary::get_armor()
{
	return ar;
}

Weapon Mary::get_weapon()
{
	return weapon;
}

int Mary::get_width() {
	return width;
}
int Mary::get_height() {
	return height;
}
bool Mary::get_beautiful() {
	return beautiful;
}
int Mary::get_number_legs() {
	return number_legs;
}
void Mary::set_armor(Armor new_armor)
{
	ar = new_armor;
}
void Mary::set_width(int new_width) {
	width = new_width;
}
void Mary::set_height(int new_height) {
	height = new_height;
}
void Mary::set_beautiful(bool new_beautiful) {
	beautiful = new_beautiful;
}
void Mary::set_number_legs(int new_number_legs) {
	number_legs = new_number_legs;
}

void Mary::set_weapon(Weapon new_weapon) {
	weapon = new_weapon;
}

void Mary::set_potion(Potion new_potion)
{
	potion = new_potion;
}

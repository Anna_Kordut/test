#pragma once
class Weapon
{
private:
	int width;
	int height;
	int lvl;
	int force;


public:
	Weapon();
	int get_width();
	int get_height();
	int get_force();
	int get_lvl();
	
	void set_width(int new_width);
	void set_height(int new_height);
	void set_lvl(int new_lvl);
	void set_force(int new_force);

};


#pragma once
#include "armor.h"
#include "weapon.h"
#include "potion.h"

class Mary {
private:
	int width;
	int height;
	bool beautiful;
	int number_legs;
	Armor ar;
	Weapon weapon;
	Potion potion;
public:
	Mary();
	Potion get_potion();
	Armor get_armor();
	Weapon get_weapon();
	int get_width();
	int get_height();
	bool get_beautiful();
	int get_number_legs();
	void set_armor(Armor new_armor);
	void set_width(int new_set_width);
	void set_height(int new_height);
	void set_beautiful(bool new_beautiful);
	void set_number_legs(int new_number_legs);
	void set_weapon(Weapon new_weapon);
	void set_potion(Potion new_potion);


};

